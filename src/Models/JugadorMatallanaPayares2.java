/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author migue
 */
public class JugadorMatallanaPayares2 extends Jugador{

    @Override
    public void EquiparArma(Arma arma) {
        armaEquipada=arma;
    }

    @Override
    public void Curar(int valorCura) {
        if(super.vida+valorCura>=100){
           super.vida=100;
        }
        else{
           super.vida=super.vida+valorCura;
        }
    }

    @Override
    public void Jugar(Mapa m, Jugador oponente) {
        int j1[],j2[];
        j1=m.obtenerCoordenadas(oponente);
        j2=m.obtenerCoordenadas(this);
        ((IOcupable)m.obtenerCasilla(j2[0],j2[1])).Desocupar();
        if (super.vida<=60){
            for(int a=0; a<50; a++){
                for(int b=0; b<50; b++){
                    if(m.obtenerCasilla(a, b)instanceof EspacioCura&&(Math.abs(j2[0]-a)<=5&&Math.abs(j2[1]-b)<=5)){
                        if (super.vida<=70){
                            Curar(15);
                        }else{
                        j2[0]=a;
                        j2[1]=b;
                        System.out.print("jugador se mueve a "+j2[0]+"--"+j2[1]+"");
                        a=50; b=50;
                        }
                    }
                }
            }
            ((IOcupable)m.obtenerCasilla(j2[0],j2[1])).Ocupar(this);
        }else{
        if(j2[0]<j1[0]) {
            j2[0]++;
        }else if (j2[0]>j1[0]){
            j2[0]--;
        }
        if(Math.abs(j1[0]-j2[0])<=armaEquipada.Rango && Math.abs(j1[1]-j2[1])<=armaEquipada.Rango){
            this.atacar(oponente);
            System.out.print("Se ha hecho un ataque");
        }
        else if(j2[1]<j1[1]) {
            j2[1]++;
            System.out.print("jugador se mueve a "+j2[0]+"--"+j2[1]+"");
        }else if (j2[1]>j1[1]){
            j2[1]--;
            System.out.print("jugador se mueve a "+j2[0]+"--"+j2[1]+"");
        }
        ((IOcupable)m.obtenerCasilla(j2[0],j2[1])).Ocupar(this);
        }
    }
    private void atacar(Jugador oponente){
        oponente.Lastimar(this.armaEquipada.Fuerza);
    }
}
