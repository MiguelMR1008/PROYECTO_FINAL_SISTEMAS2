/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Drugdu
 */
public class JugadorMatallanaPayares1 extends Jugador{
    int x=0;
    boolean inven=true;

    @Override
    public void EquiparArma(Arma arma) {
        armaEquipada=arma;
    }

    @Override
    public void Curar(int valorCura) {
        if(super.vida+valorCura>=100){
           super.vida=100;
        }
        else{
           super.vida+=valorCura;
        }
    }

    @Override
    public void Jugar(Mapa m, Jugador oponente) {
        int j1[],j2[];
            j1=m.obtenerCoordenadas(oponente);
            j2=m.obtenerCoordenadas(this);
            ((IOcupable)m.obtenerCasilla(j2[0],j2[1])).Desocupar();
        if(inven==true){
            if(super.vida>=60){
                if(j2[0]<j1[0]){
                j2[0]++;
            }else if (j2[0]>j1[0]){
                j2[0]--;
            }
            if(Math.abs(j1[0]-j2[0])<=armaEquipada.Rango && Math.abs(j1[1]-j2[1])<=armaEquipada.Rango){
                this.atacar(oponente);
                System.out.print("Se ha hecho un ataque");
            }
            else if(j2[1]<j1[1]){
                j2[1]++;
                System.out.print("jugador se mueve a "+j2[0]+"--"+j2[1]+"");
            }else if (j2[1]>j1[1]){
                j2[1]--;
                System.out.print("jugador se mueve a "+j2[0]+"--"+j2[1]+"");
            }
            ((IOcupable)m.obtenerCasilla(j2[0],j2[1])).Ocupar(this);
            }else{
                inven=false;
                armaEquipada.Fuerza=armaEquipada.Fuerza*2;
                j2[0]=3;
                j2[1]=3;
                ((IOcupable)m.obtenerCasilla(j2[0],j2[1])).Ocupar(this);
            }
        }if(inven==false){
        if(super.vida>=100){
            super.vida=100;
        }else{
            x=100-super.vida;
            super.vida=super.vida+x;
        }
        if(j2[0]<j1[0])
        {
            j2[0]=j2[0]+4;
        }else if (j2[0]>j1[0]){
            j2[0]=j2[0]-4;
        }
        if(Math.abs(j1[0]-j2[0])<=armaEquipada.Rango && Math.abs(j1[1]-j2[1])<=armaEquipada.Rango)
        {
            this.atacar(oponente);
            System.out.print("Se ha hecho un ataque");
        }
        else if(j2[1]<j1[1])
        {
            j2[1]=j2[1]+4;
            System.out.print("jugador se mueve a "+j2[0]+"--"+j2[1]+"");
        }else if (j2[1]>j1[1]){
            j2[1]=j2[1]-4;
            System.out.print("jugador se mueve a "+j2[0]+"--"+j2[1]+"");
        }
        ((IOcupable)m.obtenerCasilla(j2[0],j2[1])).Ocupar(this);
        }
    }
    private void atacar(Jugador oponente){
        oponente.Lastimar(this.armaEquipada.Fuerza);
    }
}